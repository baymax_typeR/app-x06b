package wikrama.isyana.app6b

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_mk.*
import kotlinx.android.synthetic.main.frag_data_mk.view.*

class FragmentMataKuliah : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner2.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c: Cursor = spAdapter.getItem(position) as Cursor
        namaProdi = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnDeleteMK -> {
                dialog.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnUpdateMK -> {
                dialog.setTitle("Konfirmasi").setMessage("Data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnInsertMK -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter: ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v: View
    var kodeMK: String = ""
    var namaMK: String = ""
    var namaProdi: String = ""
    lateinit var db: SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_mk, container, false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteMK.setOnClickListener(this)
        v.btnInsertMK.setOnClickListener(this)
        v.btnUpdateMK.setOnClickListener(this)
        v.spinner2.onItemSelectedListener = this
        v.lvMK.setOnItemClickListener(itemClick)

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataMK("")
        showDataProdi()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        kodeMK = c.getString(c.getColumnIndex("_id"))
        //nimMhs = c.getString(c.getColumnIndex("_id"))
        v.edNamaMK.setText(c.getString(c.getColumnIndex("namamk")))
        //v.edNimMhs.setText(c.getString(c.getColumnIndex("nim")))
    }

    fun showDataMK(namaMK: String) {
        var sql = ""
        if (!namaMK.trim().equals("")) {
            sql = "select mk.kode as _id, mk.namamk, p.nama_prodi from matkul mk, prodi p " +
                    "where mk.id_prodi=p.id_prodi and mk.namamk like '%$namaMK%'"
        } else {
            sql = "select mk.kode as _id, mk.namamk, p.nama_prodi from matkul mk, prodi p " +
                    "where mk.id_prodi=p.id_prodi order by mk.namamk asc"
        }
        val c: Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(
            thisParent,
            R.layout.item_data_mk,
            c,
            arrayOf("_id", "namamk", "nama_prodi"),
            intArrayOf(R.id.txKodeMK, R.id.txNamaMK, R.id.txNamaProdi),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lvMK.adapter = lsAdapter
    }

    fun showDataProdi() {
        val c: Cursor =
            db.rawQuery("select nama_prodi as _id from prodi order by nama_prodi asc", null)
            spAdapter = SimpleCursorAdapter(
            thisParent,
            android.R.layout.simple_spinner_item,
            c,
            arrayOf("_id"),
            intArrayOf(android.R.id.text1),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner2.adapter = spAdapter
        v.spinner2.setSelection(0)
    }

    fun insertDataMK(kode: String, namaMK: String, id_prodi: Int) {
        var sql = "insert into matkul(kode, namamk, id_prodi) values (?,?,?)"
        db.execSQL(sql, arrayOf(kode, namaMK, id_prodi))
        showDataMK("")
    }


    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_prodi from prodi where nama_prodi ='$namaProdi'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            insertDataMK(
                v.edKodeMK.text.toString(), v.edNamaMK.text.toString(),
                c.getInt(c.getColumnIndex("id_prodi"))
            )
            v.edKodeMK.setText("")
            v.edKodeMK.setText("")
        }
    }

    fun deleteDataMK(kode: String) {
        db.delete("matkul", "kode = $kode", null)
        showDataMK("")
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataMK(namaMK)
        v.edKodeMK.setText("")
        v.edNamaMK.setText("")
    }

    fun updateDataMK(kodeMK: String, namaMK: String, id_prodi: Int) {
        var sql = "update matkul set namamk='$namaMK', id_prodi = '$id_prodi' where kode='$kodeMK',"
        db.execSQL(sql, arrayOf(kodeMK, namaMK, namaProdi))
        showDataMK("")
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_prodi from prodi where nama_prodi ='$namaProdi'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            updateDataMK(
                v.edKodeMK.text.toString(), v.edNamaMK.text.toString(),
                c.getInt(c.getColumnIndex("id_prodi"))
            )
            v.edKodeMK.setText("")
            v.edNamaMK.setText("")
        }
    }
}